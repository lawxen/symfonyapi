<?php

namespace App\DataFixtures;

use App\Entity\TestUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TestUserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $testUser1 = (new TestUser())
            ->setUsername("Jim")
            ->setEmail("Jim@example.com")
            ->setPassword("password1")
            ->setIsMember(1)
            ->setIsActive(0)
            ->setUserType(2)
            ->setLastLoginAt(new \DateTime('2014-05-01 11:53:01'))
            ->setCreatedAt(new \DateTimeImmutable('2011-05-01 11:53:01'))
            ->setUpdatedAt(new \DateTime('2013-05-01 11:53:01'));

        $testUser2 = (new TestUser())
            ->setUsername("Lebron")
            ->setEmail("Lebron@example.com")
            ->setPassword("password2")
            ->setIsMember(1)
            ->setIsActive(1)
            ->setUserType(1)
            ->setLastLoginAt(new \DateTime('2013-05-01 11:53:01'))
            ->setCreatedAt(new \DateTimeImmutable('2012-05-01 11:53:01'))
            ->setUpdatedAt(new \DateTime('2012-06-01 11:53:01'));

        $testUser3 = (new TestUser())
            ->setUsername("Jordan")
            ->setEmail("Jordan@example.com")
            ->setPassword("password3")
            ->setIsMember(1)
            ->setIsActive(0)
            ->setUserType(2)
            ->setLastLoginAt(new \DateTime('2014-06-01 11:53:01'))
            ->setCreatedAt(new \DateTimeImmutable('2012-07-01 11:53:01'))
            ->setUpdatedAt(new \DateTime('2012-08-01 11:53:01'));

        $manager->persist($testUser1);
        $manager->persist($testUser2);
        $manager->persist($testUser3);

        $manager->flush();
    }
}
