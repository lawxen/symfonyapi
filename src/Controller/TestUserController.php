<?php

namespace App\Controller;

use App\Entity\TestUser;
use App\Repository\TestUserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;

/**
 * TestUserController is the controller class for the TestUser entity.
 */
class TestUserController extends AbstractController
{
    private ServiceEntityRepositoryInterface $testUserRepository;

    public function __construct(TestUserRepository $testUserRepository)
    {
        $this->testUserRepository = $testUserRepository;
    }

    /**
     * Find users by `is_active`, `is_member`, `last_login_at` (range), `user_type` (multiple values).
     * @throws Exception
     */
    #[Route('/api/test-users', name: 'api_uses', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Find all users by parameters',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: TestUser::class))
        )
    )]
    #[OA\Parameter(
        name: 'is_member',
        description: 'valid value: 0 or 1',
        in: 'query',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'is_active',
        description: 'valid value: 0 or 1',
        in: 'query',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'last_login_at',
        description: 'valid format: YYYY-MM-DD HH:MM:SS,YYYY-MM-DD HH:MM:SS, separated by comma, be sure to url encode it when you call it from code like js/php, example:2012-04-23 17:18:40,2020-10-19 23:53:01',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'user_type',
        description: 'valid value: integer, allow multiple value, like 1 or 2,3 or 1,2,3',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Tag(name: 'Test Users')]
    public function findUsers(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        // Get the query parameters
        $isMember = $request->query->get('is_member');
        $isActive = $request->query->get('is_active');
        $lastLoginAtRange = $request->query->get('last_login_at');
        $userTypes = $request->query->get('user_type');

        // Validate and sanitize the query parameters
        // Validate the $isActive parameter and converted it to integer
        if (!is_null($isActive)) {
            if (in_array($isActive, ['1', '0'], true)) {
                $isActive = (int)$isActive;
            } else {
                return new JsonResponse(
                    ['error' => 'Invalid value for is_active parameter, Valid value is 0 and 1'],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }

        // Validate the $isMember parameter and converted it to integer
        if (!is_null($isMember)) {
            if (in_array($isMember, ['1', '0'], true)) {
                $isMember = (int)$isMember;
            } else {
                return new JsonResponse(
                    ['error' => 'Invalid value for is_member parameter, Valid value is 0 and 1'],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }

        // Validate the $lastLoginAtRange and converted it to lastLoginAtStart and lastLoginAtEnd
        $lastLoginAtStart = null;
        $lastLoginAtEnd = null;
        if (!is_null($lastLoginAtRange)) {
            $lastLoginAtRange = explode(',', $lastLoginAtRange);
            // Check the $lastLoginAtRange is array and has 2 values
            if (count($lastLoginAtRange) !== 2) {
                return new JsonResponse(
                    ['error' => 'Invalid value for last_login_at parameter, Valid values is "YYYY-MM-DD HH:MM:SS,YYYY-MM-DD HH:MM:SS" format and remember to URL-encode the values if they contain special characters or spaces.'],
                    Response::HTTP_BAD_REQUEST
                );
            }
            // Check the $lastLoginAtRange is valid date time format
            foreach ($lastLoginAtRange as $dateString) {
                $format = "Y-m-d H:i:s";
                $dateTime = \DateTime::createFromFormat($format, $dateString);
                if ($dateTime && $dateTime->format($format) !== $dateString) {
                    return new JsonResponse(
                        ['error' => 'Invalid value for last_login_at parameter, Valid values is "YYYY-MM-DD HH:MM:SS,YYYY-MM-DD HH:MM:SS" format and remember to URL-encode the values if they contain special characters or spaces.'],
                        Response::HTTP_BAD_REQUEST
                    );
                }
            }
            // Convert the $lastLoginAtRange to $lastLoginAtStart and $lastLoginAtEnd with DateTime object type
            $lastLoginAtStart = new \DateTime($lastLoginAtRange[0]);
            $lastLoginAtEnd = new \DateTime($lastLoginAtRange[1]);
        }

        // Validate the $userTypes and converted it to integer array
        if (!is_null($userTypes)) {
            $userTypes = explode(',', $userTypes);
            foreach ($userTypes as $key => $userType) {
                // Check the $userType is integer
                if (filter_var($userType, FILTER_VALIDATE_INT) !== false) {
                    // Convert the $userType to integer
                    $userTypes[$key] = (int)$userType;
                } else {
                    return new JsonResponse(
                        ['error' => 'Invalid value for user_type parameter, valid value is integer and can have multiple values,like 2 or 1,2 or 1,3,2'],
                        Response::HTTP_BAD_REQUEST
                    );
                }
            }
        }

        // Find the users by the query parameters
        $users = $this->testUserRepository->findFilteredUsers(
            $isMember,
            $isActive,
            $lastLoginAtStart,
            $lastLoginAtEnd,
            $userTypes
        );

        return $this->json($users);
    }
}
