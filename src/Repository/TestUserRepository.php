<?php

namespace App\Repository;

use App\Entity\TestUser;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TestUser>
 *
 * @method TestUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestUser[]    findAll()
 * @method TestUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestUser::class);
    }

    public function save(TestUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TestUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Find filtered users by given criteria
     *
     * @param int|null $isMember
     * @param int|null $isActive
     * @param DateTime|null $lastLoginAtStart
     * @param DateTime|null $lastLoginAtEnd
     * @param array|null $userTypes
     * @return array
     */
    public function findFilteredUsers(?int $isMember, ?int $isActive, ?DateTime $lastLoginAtStart, ?DateTime $lastLoginAtEnd, ?array $userTypes): array
    {
        // Create query builder
        $qb = $this->createQueryBuilder('u');

        // Add where clauses for each of the possible filters
        if (!is_null($isMember)) {
            $qb->andWhere('u.isMember = :isMember')
                ->setParameter('isMember', $isMember);
        }

        if (!is_null($isActive)) {
            $qb->andWhere('u.isActive = :isActive')
                ->setParameter('isActive', $isActive);
        }

        if (!empty($lastLoginAtStart)) {
            $qb->andWhere('u.lastLoginAt >= :lastLoginAtStart')
                ->setParameter('lastLoginAtStart', $lastLoginAtStart);
        }

        if (!empty($lastLoginAtEnd)) {
            $qb->andWhere('u.lastLoginAt <= :lastLoginAtEnd')
                ->setParameter('lastLoginAtEnd', $lastLoginAtEnd);
        }

        if (!empty($userTypes)) {
            $qb->andWhere('u.userType IN (:userTypes)')
                ->setParameter('userTypes', $userTypes);
        }

        // Execute query and return results
        return $qb->getQuery()->getResult();
    }
}
