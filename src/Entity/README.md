1. `docker compose up -d`
1. Create the database  
   `docker exec -it symfonyapi /bin/bash -c 'symfony console doctrine:database:create'`
1. Import data from the sql file  
   `docker cp ./data/interview_data.sql mariadb-symfonyapi:/interview_data.sql`  
   `docker exec -it mariadb-symfonyapi /bin/bash -c 'mariadb -u root -ppassword app < /interview_data.sql'`
1. Execute migrations to update your database
   `docker exec -it symfonyapi /bin/bash -c 'php bin/console doctrine:migrations:migrate'`


## steps:
1. Create a new symfony6.1 project and upload to [https://gitlab.com/lawxen/symfonyapi,](https://gitlab.com/lawxen/symfonyapi,please) Please read the code first
2. the sql file is in ./data/interview_data.sql
3.  Run `composer require orm`

    `composer require symfony/orm-pack`
    `composer require --dev symfony/maker-bundle`

4. config sql connection info in .env
   `DATABASE_URL="mysql://root:password@mariadb-symfonyapi:3306/app?serverVersion=11.0.2-MariaDB&charset=utf8mb4"`
5. use `docker exec -it symfonyapi /bin/bash -c 'symfony console doctrine:database:create’` to create the database
6. Import data from the sql file  
   `docker cp ./data/interview_data.sql mariadb-symfonyapi:/interview_data.sql`
   `docker exec -it mariadb-symfonyapi /bin/bash -c 'mariadb -u root -ppassword app < /interview_data.sql'`
7. Run `php bin/console make:entity` to Create the TestUser entity
8. Run `php bin/console make:migration` and `php bin/console doctrine:migrations:migrate` to Update datase
9. then?