<?php

namespace App\Tests\Entity;

use App\Entity\TestUser;
use PHPUnit\Framework\TestCase;

/**
 * Unit test fot TestUser entity
 */
class TestUserTest extends TestCase
{
    // Test username
    public function testUsername()
    {
        $user = new TestUser();
        $userName = 'Jim';
        $user->setUsername($userName);
        $this->assertEquals($userName, $user->getUsername());
    }

    // Test email
    public function testEmail()
    {
        $user = new TestUser();
        $email = 'Jim@outlook.com';
        $user->setEmail($email);
        $this->assertEquals($email, $user->getEmail());
    }

    // TODO: test password, It's another scope, so omit it here

    // Test isMember
    public function testIsMember()
    {
        $user = new TestUser();
        $user->setIsMember(true);
        $this->assertTrue($user->isIsMember());
    }

    // Test isActive
    public function testIsActive()
    {
        $user = new TestUser();
        $user->setIsActive(true);
        $this->assertTrue($user->isIsActive());
    }

    // Test lastLoginAt
    public function testLastLoginAt()
    {
        $user = new TestUser();
        $date = new \DateTime('2022-03-30 05:27:00');
        $user->setLastLoginAt($date);
        $this->assertEquals($date, $user->getLastLoginAt());
    }

    // Test createdAt
    public function testCreatedAt()
    {
        $user = new TestUser();
        $date = new \DateTimeImmutable('2022-03-30 05:27:00');
        $user->setCreatedAt($date);
        $this->assertEquals($date, $user->getCreatedAt());
    }

    // Test updatedAt
    public function testUpdatedAt()
    {
        $user = new TestUser();
        $date = new \DateTime('2022-03-30 05:27:00');
        $user->setUpdatedAt($date);
        $this->assertEquals($date, $user->getUpdatedAt());
    }

    // Test userType
    public function testUserType()
    {
        $user = new TestUser();
        $user->setUserType(1);
        $this->assertEquals(1, $user->getUserType());
    }
}
