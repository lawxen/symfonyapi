<?php

namespace App\Tests\Repository;

use App\Entity\TestUser;
use App\Repository\TestUserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TestUserRepositoryTest extends KernelTestCase
{
    /** @var EntityManager */
    private $entityManager;

    /** @var TestUserRepository */
    private $repository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->repository = $this->entityManager->getRepository(TestUser::class);
    }

    /**
     * Test the findFilteredUsers function in TestUserRepository.
     *
     */
    public function testFindFilteredUsers()
    {
        self::bootKernel();

        // Prepare the test parameters
        $isMember = '1';
        $isActive = '0';
        $lastLoginAtStart = new \DateTime('2013-04-01 11:53:01');
        $lastLoginAtEnd = new \DateTime('2014-05-02 17:18:40');
        $userTypes = [1, 2];

        // Call findFilteredUsers to find the users
        $users = $this->repository->findFilteredUsers($isMember, $isActive, $lastLoginAtStart, $lastLoginAtEnd, $userTypes);

        // Some test on the returned users
        $this->assertNotEmpty($users);
        foreach ($users as $user) {
            $this->assertTrue($user->isIsMember());
            $this->assertFalse($user->isIsActive());
            $this->assertGreaterThanOrEqual($lastLoginAtStart, $user->getLastLoginAt());
            $this->assertLessThanOrEqual($lastLoginAtEnd, $user->getLastLoginAt());
            $this->assertContains($user->getUserType(), $userTypes);
        }
    }

    /**
     * Tear down every thing.
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
