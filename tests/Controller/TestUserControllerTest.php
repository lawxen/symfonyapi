<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestUserControllerTest extends WebTestCase
{
    /**
     * Test the findUsers function in TestUserController.
     */
    public function testFindUsers(): void
    {
        $client = static::createClient();

        // Test without any query parameters
        // Make the request
        $client->request('GET', '/api/test-users');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);
        $this->assertIsArray($responseData);
        $this->assertNotEmpty($responseData);
        // Check the structure of the response
        foreach ($responseData as $user) {
            $this->assertArrayHasKey('id', $user);
            $this->assertArrayHasKey('username', $user);
            $this->assertArrayHasKey('email', $user);
            $this->assertArrayHasKey('password', $user);
            $this->assertArrayHasKey('isMember', $user);
            $this->assertArrayHasKey('isActive', $user);
            $this->assertArrayHasKey('userType', $user);
            $this->assertArrayHasKey('lastLoginAt', $user);
            $this->assertArrayHasKey('createdAt', $user);
            $this->assertArrayHasKey('updatedAt', $user);
        }

        // Test with valid is_active query parameter
        // Make the request
        $client->request('GET', '/api/test-users?is_active=0');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);
        $this->assertIsArray($responseData);
        $this->assertNotEmpty($responseData);
        // Check the structure of the response
        foreach ($responseData as $user) {
            $this->assertArrayHasKey('isActive', $user);
            $this->assertFalse($user['isActive']);
        }

        // Test with invalid is_active query parameter
        $client->request('GET', '/api/test-users?is_active=2');
        $this->assertResponseStatusCodeSame(400);

        // Test with valid is_member query parameter
        // Make the request
        $client->request('GET', '/api/test-users?is_member=1');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);
        $this->assertIsArray($responseData);
        $this->assertNotEmpty($responseData);
        // Check the structure of the response
        foreach ($responseData as $user) {
            $this->assertArrayHasKey('isMember', $user);
            $this->assertTrue($user['isMember']);
        }

        // Test with invalid is_member query parameter
        $client->request('GET', '/api/test-users?is_member=2');
        $this->assertResponseStatusCodeSame(400);

        // Test with valid last_login_at query parameter
        // Prepare the query parameters and url
        $lastLoginAtStartString = '2013-04-01 11:53:01';
        $lastLoginAtEndString = '2014-05-02 17:18:40';
        $lastLoginAtStart = new \DateTime('2013-04-01 11:53:01');
        $lastLoginAtEnd = new \DateTime('2014-05-02 17:18:40');
        $requestUrl = sprintf(
            '/api/test-users?last_login_at=%s,%s',
            urlencode($lastLoginAtStartString),
            urlencode($lastLoginAtEndString)
        );
        // Make the request
        $client->request('GET', $requestUrl);
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);
        $this->assertIsArray($responseData);
        $this->assertNotEmpty($responseData);
        // Check the structure of the response
        foreach ($responseData as $user) {
            $this->assertArrayHasKey('lastLoginAt', $user);
            $lastLoginAt = new \DateTime($user['lastLoginAt']);
            // Check that the last_login_at is between the start and end dates
            $this->assertGreaterThanOrEqual($lastLoginAtStart, $lastLoginAt);
            $this->assertLessThanOrEqual($lastLoginAtEnd, $lastLoginAt);
        }

        // Test with invalid last_login_at query parameter
        // Make the request
        $client->request('GET', '/api/test-users?last_login_at=2013-04-01');
        $this->assertResponseStatusCodeSame(400);

        // Test with valid user_type query parameter
        // Make the request
        $client->request('GET', '/api/test-users?user_type=1,2');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);
        $this->assertIsArray($responseData);
        $this->assertNotEmpty($responseData);
        // Check the structure of the response
        foreach ($responseData as $user) {
            $this->assertArrayHasKey('userType', $user);
            $this->assertContains($user['userType'], [1, 2]);
        }

        // Test with invalid user_type query parameter
        // Make the request
        $client->request('GET', '/api/test-users?user_type=a,b');
        $this->assertResponseStatusCodeSame(400);
    }
}
