## This has been deployed to http://124.222.49.119:8085/api/doc, you can visit it directly.  
## The gitlab ci has been configured to run the test and code standard check
**The ci runs on every commit change and the run list can be found on this project's** [**pipelines**](https://gitlab.com/lawxen/symfonyapi/-/pipelines).

*Note*: In [interview_data.sql](data/interview_data.sql), I changed the table name test_users to test_user since it shouldn't be plural.

## What has been done on this project:
* Create the [TestUser](/src/Entity/TestUser.php) entity
* Create the [TestUserRepository](/src/Repository/TestUserRepository.php) which contain the query to get the TestUser data
* Create the [TestUserController](/src/Controller/TestUserController.php) which define the api/test-users endpoint openapi doc annotation
* A phpunit test [TestUserTest](/tests/Entity/TestUserTest.php) for the TestUser
* An Integration test(kernel test) [TestUserRepositoryTest](/tests/Repository/TestUserRepositoryTest.php) for the TestUserRepository
* An Application test [TestUserControllerTest](/tests/Controller/TestUserControllerTest.php) for the TestUserController
* Use [DoctrineFixturesBundle](https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html) to load the test data from [TestUserFixtures](src/DataFixtures/TestUserFixtures.php)
* Use NelmioApiDocBundle to generate the api doc from the Annotation of [TestUserController](/src/Controller/TestUserController.php)

## Project Init
1. `docker compose up -d`
1. Install the dependencies  
   `docker exec -it symfonyapi /bin/bash -c 'composer install'`
1. Create the database  
   `docker exec -it symfonyapi /bin/bash -c 'symfony console doctrine:database:create'`
1. Import data from the sql file  
   `docker cp ./data/interview_data.sql mariadb-symfonyapi:/interview_data.sql`  
   `docker exec -it mariadb-symfonyapi /bin/bash -c 'mariadb -u root -ppassword app < /interview_data.sql'`
1. Execute migrations to update your database  
   `docker exec -it symfonyapi /bin/bash -c 'php bin/console doctrine:migrations:migrate'`
1. Visit the docs on 127.0.0.1:8085/api/doc

## How to execute the test:
The test has been configured to run on the gitlab ci, you can find the test results on this project's [pipelines](https://gitlab.com/lawxen/symfonyapi/-/pipelines).  

Of course, you can also run the test locally by following the steps below:
### Prepare the test data
1. create the test database  
   `docker exec -it symfonyapi /bin/bash -c 'php bin/console --env=test doctrine:database:create'`
1. create the tables/columns in the test database  
    `docker exec -it symfonyapi /bin/bash -c 'php bin/console --env=test doctrine:schema:create'`
1. Empty the database and reload all the fixture classes to generate example data  
   `docker exec -it symfonyapi /bin/bash -c 'php bin/console --env=test doctrine:fixtures:load --no-interaction'`
### [TestUserTest](/tests/Entity/TestUserTest.php)
1. `docker exec -it symfonyapi /bin/bash -c 'php bin/phpunit tests/Entity/TestUserTest.php'`
1. ### [TestUserRepositoryTest](/tests/Repository/TestUserRepositoryTest.php)
   `docker exec -it symfonyapi /bin/bash -c 'php bin/phpunit tests/Repository/TestUserRepositoryTest.php'`
1. ### [TestUserControllerTest](/tests/Controller/TestUserControllerTest.php)
   `docker exec -it symfonyapi /bin/bash -c 'php bin/phpunit tests/Controller/TestUserControllerTest.php'`  

## Performance: Mariadb indexes
1. is_active, is_member and user_type is int and only a few distinct values, no need to add index on them.
2. last_login_at is datetime, In a real use case，the field is frequently updated, adding an index may impact the performance of insert and update operations.


